<?php

namespace Drupal\webform_remote_fields\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Command to trigger change event to the element update by ajax.
 */
class RemoteFieldTriggerChangeValue implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * If the command is a response to a request from an #ajax form element then
   * this value can be NULL.
   *
   * @var string
   */
  protected string $selector;

  /**
   * Construct RemoteRemoteFieldTriggerChangeValue.
   *
   * @param string $selector
   *   Element selector to be triggered.
   */
  public function __construct(string $selector) {
    $this->selector = $selector;
  }

  /**
   * {@inheritDoc}
   */
  public function render() {
    return [
      'command' => 'webformRemoteFieldTriggerChangeValue',
      'selector' => $this->selector,
    ];
  }

}
