<?php

namespace Drupal\webform_remote_fields\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Hidden;
use Drupal\webform_remote_fields\WebformElementRemoteFieldTrait;

/**
 * Provides a 'Webform Remote Fields Hidden' element.
 *
 * @WebformElement(
 *   id = "remote_field_hidden",
 *   label = @Translation("Remote Hidden"),
 *   description = @Translation("Provides a hidden form element with API Integration."),
 *   category = @Translation("Remote Fields"),
 * )
 */
class WebformRemoteFieldsHidden extends Hidden {

  use WebformElementRemoteFieldTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return $this->remoteFieldBaseProperties()
      + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->baseFormFields($form);
    return $form;
  }

}
