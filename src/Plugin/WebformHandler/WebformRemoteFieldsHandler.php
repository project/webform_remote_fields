<?php

namespace Drupal\webform_remote_fields\Plugin\WebformHandler;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_remote_fields\Ajax\RemoteFieldTriggerChangeValue;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom API Webform Handler.
 *
 * @WebformHandler(
 *   id = "webform_remote_fields_handler",
 *   label = @Translation("Webform Remote Fields Handler"),
 *   category = @Translation("Remote Fields"),
 *   description = @Translation("Handles the custom API fields and special alters."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 * )
 */
class WebformRemoteFieldsHandler extends WebformHandlerBase {

  /**
   * Drupal token service container.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $tokenService;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenService = $container->get('token');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    if (empty($form['elements'])) {
      return;
    }
    $fieldsThatDependsOnOtherFields = $this->getElementsThatUseOtherFieldsOnApi($form['elements']);
    if (empty($fieldsThatDependsOnOtherFields)) {
      return;
    }

    foreach ($fieldsThatDependsOnOtherFields as $element_id => $blockedElements) {
      // Add AJAX to every element that has been set as "dependency" or API
      // param in another element.
      $element = &WebformElementHelper::getElement($form['elements'], $element_id);
      $element['#ajax'] = [
        'callback' => [$this, 'ajaxCallback'],
        'event' => 'change',
        'progress' => ['type' => 'throbber'],
      ];
      $fieldName = $element['#name'] ?? $element["#webform_key"];
      $element['#attributes']['data-blocks-this-element'] = $blockedElements;
      $triggeredElement = $form_state->getTriggeringElement();
      if (!empty($triggeredElement)) {
        $triggeredElementId = $triggeredElement['#name'] ?? $triggeredElement["#webform_key"];
        if ($triggeredElementId === $fieldName) {
          $form_state->setTriggeringElement($element);
        }
      }
    }
  }

  /**
   * AJAX callback function.
   */
  public function ajaxCallback(array &$form, FormStateInterface $formState) {
    $blockedElementMachineName = $formState->getTriggeringElement()['#attributes']['data-blocks-this-element'];
    $dependentFieldsOfTheFieldUpdated = $this->getElementsThatUseOtherFieldsOnApi($form['elements']);
    $response = new AjaxResponse();
    foreach ($blockedElementMachineName as $key) {
      $element = &WebformElementHelper::getElement($form['elements'], $key);
      // Update element only if it's rendered on the page.
      if (!$element['#access']) {
        continue;
      }

      $this->addAjaxCommand($response, $element);

      $dependentFields = $dependentFieldsOfTheFieldUpdated[$key];
      if (!empty($dependentFields)) {
        foreach ($dependentFields as $dependentKey) {
          $dependentElement = &WebformElementHelper::getElement($form['elements'], $dependentKey);
          if (!$dependentElement['#access']) {
            continue;
          }
          $this->addAjaxCommand($response, $dependentElement);
        }
      }
    }
    return $response;
  }

  /**
   * Add the command ajax of element into the ajax response.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   The response to have the command added.
   * @param array $element
   *   The element to be updated.
   */
  private function addAjaxCommand(AjaxResponse &$response, array $element): void {
    $id = $dependentElement['#name'] ?? $element['#webform_key'];
    $response->addCommand(new ReplaceCommand('#' . $id . '-wrapper', $element));
    if (!empty($element["#name"])) {
      $response->addCommand(new RemoteFieldTriggerChangeValue('[name="' . $element["#name"] . '"]'));
    }
  }

  /**
   * Get elements id that use other fields.
   *
   * @param array $elements
   *   List of webform elements.
   *
   * @return array
   *   Array with elements id.
   */
  public function getElementsThatUseOtherFieldsOnApi(array $elements = []): array {
    if (empty($elements)) {
      return [];
    }
    $elementIds = [];

    $allPlugins = \Drupal::service('webform_remote_fields.helper')
      ->getElements();
    $elementTypes = array_keys($allPlugins);

    foreach ($elements as $key => $element) {

      if (empty($element['#type'])) {
        continue;
      }

      // If the current element has children (sub-elements), recursively search
      // within them and merge results.
      $containerTypes = ['container', 'webform_wizard_page', 'fieldset', 'details', 'fieldset', 'webform_flexbox', 'webform_section'];
      if (in_array($element['#type'], $containerTypes) && is_array($element)) {
        $elementIds = array_merge_recursive($elementIds, $this->getElementsThatUseOtherFieldsOnApi($element));
      }

      // Check dependencies only for the remote fields.
      if (!in_array($element['#type'], $elementTypes)) {
        continue;
      }

      if (!empty($element['#api_endpoint_params'])) {
        // Get elements defined as parameter.
        foreach ($element['#api_endpoint_params'] as $param) {
          $hasToken = $this->tokenService->scan($param['param_value'] . ' ' . $param['param_name']);
          if (empty($hasToken)) {
            continue;
          }

          $tokens = $this->tokenService->scan($param['param_value']);
          if (!empty($tokens['form_value'])) {
            foreach ($tokens['form_value'] as $fieldName => $value) {
              $values = explode(':', $fieldName);
              if (count($values) === 2) {
                $dependent_field = $values[1];
                $elementIds[$dependent_field][] = $key;
              }
            }
          }

          $tokens = $this->tokenService->scan($param['param_name']);
          if (!empty($tokens['form_value'])) {
            foreach ($tokens['form_value'] as $fieldName => $value) {
              $values = explode(':', $fieldName);
              if (count($values) === 2) {
                $dependent_field = $values[1];
                $elementIds[$dependent_field][] = $key;
              }
            }
          }
        }
      }

      $settings = [
        '#api_endpoint',
        '#api_result_select_value',
        '#api_result_select_text',
        '#headers',
      ];

      foreach ($settings as $setting) {
        if (empty($element[$setting])) {
          continue;
        }
        $tokens = $this->tokenService->scan($element[$setting]);
        if (!empty($tokens['form_value'])) {
          foreach ($tokens['form_value'] as $dependent_field => $token) {
            $values = explode(':', $dependent_field);
            if (count($values) === 2) {
              $dependent_field = $values[1];
              $elementIds[$dependent_field][] = $key;
            }
          }
        }
      }
    }

    return $elementIds;
  }

}
