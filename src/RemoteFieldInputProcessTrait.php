<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provide remote field feature for text/number inputs.
 */
trait RemoteFieldInputProcessTrait {

  use RemoteFieldBaseTrait;

  /**
   * {@inheritdoc}
   */
  public static function processAjaxForm(&$element, FormStateInterface $formState, &$completeForm) {
    // Check if an element is on the current wizard page.
    // This has no effect if there aren't any pages.
    $currentStep = $completeForm["progress"]["#current_page"] ?? NULL;
    if ($currentStep !== NULL && !in_array($currentStep, $element['#webform_parents'], TRUE)) {
      return $element;
    }

    $apiSettings = static::extractElementSettings($element, $formState);
    $element['#prefix'] = "<div id='{$element['#name']}-wrapper'>";
    $element['#suffix'] = "</div>";

    $response = static::processElementRequest($element, $formState);

    if (empty($response['data'])) {
      if ($response['error']) {
        $formState->setError($element, t('An unexpected error occurred, please try later'));
      }
      $element['#value'] = "";
      return parent::processAjaxForm($element, $formState, $completeForm);
    }
    $data = $response['data'][$apiSettings['api_result_select_value']] ?? $response['data'][0][$apiSettings['api_result_select_value']];
    if (!empty($apiSettings["api_round_result"])) {
      $data = number_format($data, $apiSettings["api_round_result"]);
    }
    $element['#value'] = $data;
    $formState->setValue($element['#name'], $data);
    return parent::processAjaxForm($element, $formState, $completeForm);
  }

}
