<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_remote_fields\Event\WebformRemoteFieldAlterResponseEvent;
use Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper;
use Symfony\Component\Yaml\Yaml;

/**
 * Provide shared features between Webform Remote Field elements.
 */
trait RemoteFieldBaseTrait {

  public static function tokenHelper(): WebformRemoteFieldHelper {
    return \Drupal::service('webform_remote_fields.helper');
  }

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#process'][] = [
      static::class,
      'processLibrary',
    ];
    return $info;
  }

  /**
   * Attach library to trigger element change.
   */
  public static function processLibrary($element, FormStateInterface $form_state, $form) {
    $element['#attached']['library'][] = 'webform_remote_fields/webform_remote_fields';
    return $element;
  }

  /**
   * Make the requests for each remote field element.
   */
  protected static function processElementRequest(&$element, FormStateInterface $formState) {
    $result = [
      'error' => FALSE,
      'data' => NULL,
    ];
    $requestOptions = [];
    $tokenService = \Drupal::token();
    $tokenHelper = self::tokenHelper();

    // Get api settings from the element to make the request.
    $apiSettings = self::extractElementSettings($element, $formState);
    $endpoint = $apiSettings['api_endpoint'];

    // If the props to read from request uses tokens replace for the values.
    $resultSelectTextField = $tokenHelper->getFieldsFromToken($apiSettings["api_result_select_text"] ?? "");
    if (!empty($resultSelectTextField)) {
      $value = $formState->getValue($resultSelectTextField[0]);
      if (empty($value)) {
        return $result;
      }
      $element['#api_result_select_text'] = $value;
    }
    $resultSelectValueField = $tokenHelper->getFieldsFromToken($apiSettings["api_result_select_value"] ?? "");
    if (!empty($resultSelectValueField)) {
      $value = $formState->getValue($resultSelectValueField[0]);
      if (empty($value)) {
        return $result;
      }
      $element['#api_result_select_value'] = $value;
    }

    // Build headers.
    if ($apiSettings["needs_header"]) {
      $headers = $tokenHelper->buildHeaderRequestOptions($element['#headers'], $formState);
      if (!empty($headers)) {
        $requestOptions['headers'] = $headers;
      }
    }

    // Check query params.
    if (!empty($element['#api_endpoint_params'])) {
      $queryParams = $tokenHelper->buildQueryParamsRequestTokens(
        $element['#api_endpoint_params'],
        $formState
      );
      if (!empty($queryParams)) {
        $requestOptions['query'] = $queryParams;
      }
    }

    // Build endpoint if has tokens.
    $endpointsFields = $tokenHelper->getFieldsFromToken($endpoint);
    if (!empty($endpointsFields)) {
      foreach ($endpointsFields as $fieldName) {
        $value = $formState->getValue($fieldName);
        if (empty($value)) {
          return $result;
        }
      }
      $endpoint = $tokenService->replace($endpoint, ['form_value' => $formState->getValues()]);
    }

    /** @var \Drupal\webform_remote_fields\Service\Handler $handler */
    $handler = \Drupal::service('webform_remote_fields.handler');
    $result['data'] = $handler->get($endpoint, $requestOptions, [
      'form_state' => $formState,
      'element' => $element,
    ]);

    $event = new WebformRemoteFieldAlterResponseEvent(
      $formState,
      $element,
      $result['data'] ?? [],
    );
    \Drupal::service('event_dispatcher')
      ->dispatch($event, WebformRemoteFieldAlterResponseEvent::EVENT_NAME);

    $result['data'] = $event->getResponse();
    if (is_null($result['data'])) {
      $result['error'] = TRUE;
    }
    return $result;
  }

  /**
   * Transform #api_endpoint_params setting to array.
   *
   * @param array $element
   *   Webform element.
   *
   * @return array
   *   Array where parameter value is the key, param value it's the array value.
   */
  public static function apiEndpointParametersToArray(array $element) {
    if (empty($element['#api_endpoint_params'])) {
      return [];
    }
    $map = [];
    foreach ($element['#api_endpoint_params'] as $param) {
      $map[$param['param_name']] = $param['param_value'];
    }
    return $map;
  }

  /**
   * Extract setting of the Webform remote field element.
   *
   * The order of array with the setting values.
   *  -> api_endpoint_add_query_parameter.
   *  -> api_endpoint.
   *  -> api_result_select_text.
   *  -> headers.
   *
   * @param array $element
   *   The webform element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   An object of the form state.
   *
   * @return array
   *   Array with the key => value setting.
   */
  public static function extractElementSettings(array $element, FormStateInterface $formState) {
    $tokenService = \Drupal::token();
    $apiEndpointAddQueryParameter = $element['#api_endpoint_add_query_parameter'] ?? FALSE;
    $apiEndpoint = $element['#api_endpoint'] ?? NULL;
    $apiResultSelectValue = $element['#api_result_select_value'] ?? NULL;
    $apiResultSelectText = $element['#api_result_select_text'] ?? NULL;
    $apiNeedsHeader = $element['#needs_header'] ?? NULL;

    $custom_headers = [];
    if ($apiNeedsHeader) {
      $custom_headers = Yaml::parse($element['#headers']);
    }

    // As we have the support for get the property request value from,
    // another webform field. We have to replace the tokens for
    // #api_result_select_value setting at this point.
    if (!empty($element['#api_result_select_value'])) {
      $apiResultSelectValueToken = $tokenService->replace(
        $element['#api_result_select_value'],
        ['form_value' => $formState->getValues()],
      );
      if ($apiResultSelectValueToken) {
        $apiResultSelectValue = $apiResultSelectValueToken;
      }
    }

    return [
      'api_endpoint_add_query_parameter' => $apiEndpointAddQueryParameter,
      'api_endpoint' => $apiEndpoint,
      'api_result_select_value' => $apiResultSelectValue,
      'api_result_select_text' => $apiResultSelectText,
      'custom_headers' => $custom_headers,
      'needs_header' => $apiNeedsHeader,
      'api_round_result' => $element['#api_round_result'] ?? 0,
    ];
  }

  /**
   * Check if the element is triggered.
   */
  public static function isElementTriggered(array $element, FormStateInterface $formState) {
    $triggeredElement = $formState->getTriggeringElement();
    if (empty($triggeredElement)) {
      return FALSE;
    }
    return $element['#webform_key'] === $triggeredElement['#webform_key'];
  }

}
