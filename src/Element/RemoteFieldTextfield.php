<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Render\Element\Textfield;
use Drupal\webform_remote_fields\RemoteFieldInputProcessTrait;

/**
 * Custom form element textfield.
 *
 * Provide a way to set the value getting the data from a custom request,
 * to an external or internal service.
 *
 * @FormElement("remote_field_textfield")
 */
class RemoteFieldTextfield extends Textfield {

  use RemoteFieldInputProcessTrait;

}
