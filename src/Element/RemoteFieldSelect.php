<?php

namespace Drupal\webform_remote_fields\Element;

use Drupal\Core\Render\Element\Select;
use Drupal\webform_remote_fields\RemoteFieldOptionsProcessTrait;

/**
 * Custom webform remote field.
 *
 * Webform element that populate the options based on the API parameters.
 * On the field element edit form settings a fieldset called API appears,
 * The following settings will be used to make the request.
 *
 * API Endpoint -> the url of the api to be called.
 *
 * API JSON Prop - Value -> The property name on the array item response to be,
 * used as value on the select options.
 *
 * API JSON Prop - Text -> The property name on the array item response to be,
 * used as text/label on the select options.
 *
 * @FormElement("remote_field_select")
 */
class RemoteFieldSelect extends Select {

  use RemoteFieldOptionsProcessTrait;

}
