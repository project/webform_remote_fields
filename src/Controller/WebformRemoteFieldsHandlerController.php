<?php

namespace Drupal\webform_remote_fields\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\webform\WebformEntityHandlersForm;
use Drupal\webform\WebformInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Extends WebformEntityHandlersForm::ajaxOperation adding extra validation.
 *
 * Stop disable operation if the user cannot delete or disable the handler.
 * To delete the user must have the permission
 * - administer webform_remote_fields configuration
 * Also the Webform cannot have any remote field.
 * Otherwise, the remote fields won't work properly.
 */
class WebformRemoteFieldsHandlerController extends WebformEntityHandlersForm {

  /**
   * {@inheritDoc}
   */
  public static function ajaxOperation(WebformInterface $webform, $webform_handler, $operation, Request $request) {
    // Perform the handler disable/enable operation.
    if ($webform_handler === 'webform_remote_fields_handler' && $operation === 'disable') {
      $response = new AjaxResponse();
      if (!_webform_remote_fields_user_is_admin()) {
        $message = new MessageCommand(
          t("You don't have access to delete this handler!"),
          '.tableresponsive-toggle-columns',
          [
            'type' => 'warning',
            'announce' => t("You don't have access to delete this handler!"),
          ]
        );
        $response->addCommand($message);
        return $response;
      }

      if (_webform_remote_fields_has_remote_field($webform)) {
        $message = new MessageCommand(
          t("You cannot disable this handler until all Webform Remote Field Elements are removed."),
          '.tableresponsive-toggle-columns',
          [
            'type' => 'warning',
            'announce' => t("You don't have access to delete this handler!"),
          ]
        );
        $response->addCommand($message);
        return $response;
      }
    }
    return parent::ajaxOperation($webform, $webform_handler, $operation, $request);
  }

}
