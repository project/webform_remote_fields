<?php

namespace Drupal\webform_remote_fields;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provide remote field feature for elements that work with options.
 */
trait RemoteFieldOptionsProcessTrait {

  use RemoteFieldBaseTrait;

  /**
   * Call the parent method based on the element type.
   */
  private static function executeProcessByElement(&$element, FormStateInterface $formState, &$completeForm) {
    $type = $element['#type'];
    switch ($type) {
      case 'remote_field_radios':
        return parent::processRadios($element, $formState, $completeForm);

      case 'remote_field_select':
        return parent::processSelect($element, $formState, $completeForm);

      case 'remote_field_checkboxes':
        return parent::processCheckboxes($element, $formState, $completeForm);

      default:
        return $element;
    }
  }

  /**
   * Default process element method for elements (radios, checkboxes, select).
   */
  protected static function processOptionsElement(&$element, FormStateInterface $formState, &$completeForm) {
    $element['#prefix'] = "<div id='{$element['#name']}-wrapper'>";
    $element['#suffix'] = "</div>";
    // Validate element only on submit.
    $element['#validated'] = $formState->getTriggeringElement() !== NULL;
    $response = static::processElementRequest($element, $formState);
    if (is_null($response['data'])) {
      if ($response['error']) {
        $formState->setError($element, t('An unexpected error occurred, please try later'));
      }
      // For radios and checkboxes the options must at least an array.
      if ($element['#type'] !== 'remote_field_select') {
        $element['#options'] = [];
      }
      return static::executeProcessByElement($element, $formState, $completeForm);
    }
    $element['#options'] = [];
    foreach ($response['data'] as $option) {
      $index = $option[$element['#api_result_select_value']];
      $text = $option[$element['#api_result_select_text']];
      // Populate the options for the select element based on the API response.
      $element['#options'][$index] = $text;
    }

    // Keep support to check all/none values.
    if (!empty($element["#options_all"])) {
      $element['#options'][$element["#options_all_value"]] = $element["#options_all_text"];
    }
    if (!empty($element["#options_none"])) {
      $element['#options'][$element["#options_none_value"]] = $element["#options_none_text"];
    }
    return static::executeProcessByElement($element, $formState, $completeForm);
  }

  /**
   * {@inheritDoc}
   */
  public static function processRadios(&$element, FormStateInterface $formState, &$completeForm) {
    return static::processOptionsElement($element, $formState, $completeForm);
  }

  /**
   * {@inheritDoc}
   */
  public static function processCheckboxes(&$element, FormStateInterface $formState, &$completeForm) {
    return static::processOptionsElement($element, $formState, $completeForm);
  }

  /**
   * {@inheritDoc}
   */
  public static function processSelect(&$element, FormStateInterface $formState, &$completeForm) {
    return static::processOptionsElement($element, $formState, $completeForm);
  }

}
