<?php

namespace Drupal\webform_remote_fields\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\webform_remote_fields\Controller\WebformRemoteFieldsHandlerController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Override route entity.webform.handler.disable.
 */
class WebformRemoteFieldsHandlerRouting extends RouteSubscriberBase {

  /**
   * Routes to set the new form behavior.
   *
   * @var string[]
   */
  protected static $routes = [
    'entity.webform.handler.disable' => WebformRemoteFieldsHandlerController::class,
  ];

  /**
   * {@inheritDoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach (static::$routes as $routeName => $controller) {
      $routeInfo = $collection->get($routeName);
      if ($routeInfo) {
        $routeInfo->setDefault('_controller', $controller . "::ajaxOperation");
      }
    }
  }

}
