<?php

namespace Drupal\webform_remote_fields\Service;

use Drupal\webform\WebformTokenManager;

/**
 * Override token manager to add the token type form value.
 */
class WebformRemoteFieldTokenManager extends WebformTokenManager {

  /**
   * {@inheritDoc}
   */
  public function elementValidate(array &$form, array $token_types = [
    'webform',
    'webform_submission',
    'webform_handler',
  ]) {
    $token_types = array_merge($token_types, [
      'form_value',
    ]);
    parent::elementValidate($form, $token_types);
  }

}
