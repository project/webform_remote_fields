<?php

namespace Drupal\webform_remote_fields\Service;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;

/**
 * Helper class to provide feature about remote field elements.
 */
class WebformRemoteFieldHelper {

  /**
   * Constructor.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
   *   The plugin manager of the custom elements.
   */
  public function __construct(
    private PluginManagerInterface $pluginManager,
    private Token $token,
  ) {}

  /**
   * Get all custom remote field elements.
   *
   * @return array
   *   An array with elements.
   */
  public function getElements(): array {
    return array_filter($this->pluginManager->getDefinitions(), function ($definition) {
      return isset($definition['provider']) && $definition['provider'] === 'webform_remote_fields';
    });
  }

  /**
   * Get fields from token.
   *
   * @param string $source
   *   The string to scan the tokens.
   *
   * @return array
   *   Array of field.
   */
  public function getFieldsFromToken(string $source): array {
    $fields = [];
    $tokens = $this->token->scan($source);
    if (empty($tokens['form_value'])) {
      return $fields;
    }

    foreach ($tokens['form_value'] as $fieldName => $value) {
      $values = array_filter(explode(':', $fieldName));
      if (count($values) === 2) {
        $fields[] = $values[1];
      }
    }
    return $fields;
  }

  /**
   * Build the header replacing token for the values.
   *
   * @param string $headersSettings
   *   The header settings.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state data.
   *
   * @return array|null
   *   Array of header options or null if a field is not completed.
   */
  public function buildHeaderRequestOptions(string $headersSettings, FormStateInterface $formState): array|null {
    $headers = [];
    $headerLine = explode("\n", $headersSettings);
    foreach ($headerLine as $headerValue) {
      $fields = $this->getFieldsFromToken($headerValue);
      // If any token found add as plain text in header props.
      if (empty($fields)) {
        [$headerKey, $headerKeyValue] = explode(':', $headerValue);
        $headers[trim($headerKey)] = trim($headerKeyValue);
        continue;
      }
      foreach ($fields as $fieldName) {
        $fieldValue = $formState->getValue($fieldName);
        if (empty($fieldValue)) {
          return NULL;
        }
        $headerLineReplaced = $this->token->replace($headerValue, ['form_value' => $formState->getValues()]);
        [$headerKey, $headerKeyValue] = explode(':', $headerLineReplaced);
        $headers[trim($headerKey)] = trim($headerKeyValue);
      }
    }
    return $headers;
  }

  /**
   * Build query params replacing tokens to the values.
   *
   * @param array $params
   *   Params defined to the request.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   *
   * @return array|null
   *   Array of query options or null if a field is not completed.
   */
  public function buildQueryParamsRequestTokens(array $params, FormStateInterface $formState): array|null {
    $queryParams = [];
    foreach ($params as $param) {

      // Replace if need param_name.
      $paramName = $param["param_name"];
      $tokens = $this->token->scan($param["param_name"]);
      if (!empty($tokens['form_value'])) {
        foreach ($tokens['form_value'] as $fieldName => $token) {
          $fieldName = explode(":", $fieldName)[1];
          $fieldValue = $formState->getValue($fieldName);
          if (empty($fieldValue)) {
            return NULL;
          }
          $paramName = $this->token->replace($paramName, ['form_value' => $formState->getValues()]);
        }
      }

      // Replace if need param_value.
      $paramValue = $param["param_value"];
      $tokens = $this->token->scan($param["param_value"]);
      if (!empty($tokens['form_value'])) {
        foreach ($tokens['form_value'] as $fieldName => $token) {
          $fieldName = explode(":", $fieldName)[1];
          $fieldValue = $formState->getValue($fieldName);
          if (empty($fieldValue)) {
            return NULL;
          }
          $paramValue = $this->token->replace($paramValue, ['form_value' => $formState->getValues()]);
        }
      }
      $queryParams[$paramName] = $paramValue;
    }
    return $queryParams;
  }

}
