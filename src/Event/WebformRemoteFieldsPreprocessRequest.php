<?php

namespace Drupal\webform_remote_fields\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Form\FormStateInterface;

/**
 * Custom event triggered before send the request for the remote fields.
 *
 * Subscribing this event it's possible to manipulate the data before,
 * the request be sent.
 * Sometimes the rest service can expected the parameters or any other,
 * configuration different of the default behavior sent by the module.
 * Here it's a possibility to adapt the request to desired rest service.
 */
class WebformRemoteFieldsPreprocessRequest extends Event {

  const EVENT_NAME = 'webform_remote_fields_preprocess_request';

  /**
   * The current form state of the Webform of the element triggered the request.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $formState;

  /**
   * The element data triggered the request.
   *
   * @var array
   */
  protected array $element;

  /**
   * Endpoint url.
   *
   * @var string
   */
  protected string $uri;

  /**
   * Array with the options to use on the request.
   *
   * @var array
   */
  protected array $options;

  /**
   * {@inheritDoc}
   */
  public function __construct(FormStateInterface $formState, array $element, $uri, $options = []) {
    $this->formState = $formState;
    $this->element = $element;
    $this->uri = $uri;
    $this->options = $options;
  }

  /**
   * Get the form state of the current webform.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   Form state instance of the Webform.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

  /**
   * Get element triggered.
   *
   * @return array
   *   An array with element property.
   */
  public function getElement(): array {
    return $this->element;
  }

  /**
   * The endpoint uri.
   *
   * @return string
   *   A string with endpoint value.
   */
  public function getUri(): string {
    return $this->uri;
  }

  /**
   * The options to be used on the request.
   *
   * @return array
   *   An array with key / values of the request option.
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * Set the uri to be used on the request.
   *
   * @param string $uri
   *   A string to the endpoint.
   *
   * @return WebformRemoteFieldsPreprocessRequest
   *   It's self.
   */
  public function setUri(string $uri): WebformRemoteFieldsPreprocessRequest {
    $this->uri = $uri;
    return $this;
  }

  /**
   * Set the options to be used on the request.
   *
   * @param array $options
   *   Array of each option to be used.
   *
   * @return WebformRemoteFieldsPreprocessRequest
   *   It's self.
   */
  public function setOptions(array $options): WebformRemoteFieldsPreprocessRequest {
    $this->options = $options;
    return $this;
  }

}
