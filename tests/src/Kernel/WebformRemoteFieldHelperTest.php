<?php

namespace Drupal\Tests\webform_remote_fields\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper;

/**
 * Test service WebformRemoteFieldHelper.
 *
 * @covers \Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper
 * @group webform_remote_fields
 */
class WebformRemoteFieldHelperTest extends KernelTestBase {

  protected static $modules = [
    'webform_remote_fields',
    'webform',
    'user',
  ];

  /**
   * Service to test.
   *
   * @var \Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper|mixed
   */
  protected WebformRemoteFieldHelper $webformRemoteFieldHelper;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->webformRemoteFieldHelper = \Drupal::service('webform_remote_fields.helper');
  }

  /**
   * Get fields from token.
   *
   * @dataProvider dataProviderGetFieldsFromToken
   * @covers \Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper::getFieldsFromToken
   */
  public function testGetFieldsFromToken(string $source, array $expectedResult) {
    $result = $this->webformRemoteFieldHelper->getFieldsFromToken($source);
    $this->assertEquals($expectedResult, $result);
  }

  /**
   * Data provider for getFieldsFromToken.
   *
   * @return \Generator
   */
  public function dataProviderGetFieldsFromToken() {
    yield [
      '',
      [],
    ];
    yield [
      '[form_value:name:]',
      [],
    ];
    yield [
      '[form_value:name:some_field]',
      ['some_field'],
    ];
    yield [
      'http://example.com/endpoint-x/[form_value:name:some_field]/another-path/[form_value:name:second_field]',
      ['some_field', 'second_field'],
    ];
  }

  /**
   * @dataProvider dataProviderBuildHeaderRequestOptions
   * @covers \Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper::buildHeaderRequestOptions
   *
   * @return void
   */
  public function testBuildHeaderRequestOptions($element, FormStateInterface $formState, $expectedResult) {
    $result = $this->webformRemoteFieldHelper
      ->buildHeaderRequestOptions($element, $formState);
    $this->assertEquals($expectedResult, $result);
  }

  /**
   * Data provider for testBuildHeaderRequestOptions.
   *
   * @return \Generator
   */
  public function dataProviderBuildHeaderRequestOptions() {
    $formState = new FormState();
    $formState->setValue('basic_auth', 'basic_auth_value_example');
    yield [
      "Authorization: [form_value:name:basic_auth]\nAnother-Header: some value",
      $formState,
      [
        'Authorization' => 'basic_auth_value_example',
        'Another-Header' => 'some value',
      ],
    ];
    yield [
      "Authorization: [form_value:name:basic_auth]\nAnother-Header: some value",
      new FormState(),
      NULL,
    ];
  }

  /**
   * Get build query params.
   *
   * @dataProvider dataProviderBuildQueryParamsRequestTokens
   * @covers \Drupal\webform_remote_fields\Service\WebformRemoteFieldHelper::buildQueryParamsRequestTokens
   */
  public function testBuildQueryParamsRequestTokens(array $params, array $formStateValues, $expectedResult) {
    $formStateMock = new FormState();
    foreach ($formStateValues as $field => $value) {
      $formStateMock->setValue($field, $value);
    }

    $result = $this->webformRemoteFieldHelper
      ->buildQueryParamsRequestTokens($params, $formStateMock);
    $this->assertEquals($expectedResult, $result);
  }

  /**
   * Data provider for testBuildQueryParamsRequestTokens.
   *
   * @return array[]
   */
  public function dataProviderBuildQueryParamsRequestTokens(): array {
    return [
      'no tokens in params' => [
        'params' => [
          ['param_name' => 'static_name', 'param_value' => 'static_value'],
        ],
        'formStateValues' => [],
        'expectedResult' => ['static_name' => 'static_value'],
      ],
      'token in param_name' => [
        'params' => [
          ['param_name' => '[form_value:name:field1]', 'param_value' => 'static_value'],
        ],
        'formStateValues' => ['field1' => 'replaced_name'],
        'expectedResult' => ['replaced_name' => 'static_value'],
      ],
      'token in param_value' => [
        'params' => [
          ['param_name' => 'static_name', 'param_value' => '[form_value:name:field1]'],
        ],
        'formStateValues' => ['field1' => 'replaced_value'],
        'expectedResult' => ['static_name' => 'replaced_value'],
      ],
      'token not replaced due to empty field' => [
        'params' => [
          ['param_name' => '[form_value:name:field1]', 'param_value' => 'static_value'],
        ],
        'formStateValues' => [],
        'expectedResult' => null,
      ],
      'multiple tokens in params' => [
        'params' => [
          ['param_name' => '[form_value:name:field1]', 'param_value' => '[form_value:name:field2]'],
        ],
        'formStateValues' => ['field1' => 'replaced_name', 'field2' => 'replaced_value'],
        'expectedResult' => ['replaced_name' => 'replaced_value'],
      ],
    ];
  }

}
