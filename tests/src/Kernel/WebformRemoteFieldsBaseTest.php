<?php

namespace Drupal\Tests\webform_remote_fields\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\webform_remote_fields\Element\RemoteFieldMarkup;

/**
 * Test remote field element setting submit.
 * @group webform_remote_fields
 */
class WebformRemoteFieldsBaseTest extends KernelTestBase {

  /**
   * Modules to be enabled.
   *
   * @var string[]
   */
  protected static $modules = [
    'webform_remote_fields',
    'webform',
    'user',
  ];

  /**
   * Test dependency values.
   *
   * @dataProvider dataProviderGetFieldDependenciesValues
   */
  public function testGetFieldDependenciesValues($element, $expected_settings) {
    $items = RemoteFieldMarkup::apiEndpointParametersToArray($element);
    $this->assertEquals($items, $expected_settings);
  }

  /**
   * Data provider for testGetFieldDependenciesValues.
   *
   * @return \Generator
   *   All data to test the scenarios.
   */
  public function dataProviderGetFieldDependenciesValues() {
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
        ],
      ],
      ['param_1' => 'field_x'],
    ];
    yield [
      [
        '#api_endpoint_params' => [
          ['param_name' => 'param_1', 'param_value' => 'field_x'],
          ['param_name' => 'param_2', 'param_value' => 'field_y'],
        ],
      ],
      [
        'param_1' => 'field_x',
        'param_2' => 'field_y',
      ],
    ];
  }

}
