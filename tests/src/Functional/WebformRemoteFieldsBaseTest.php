<?php

namespace Drupal\Tests\webform_remote_fields\Functional;

use Drupal\Core\Config\FileStorage;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Base class to test our custom form elements.
 * @group webform_remote_fields
 */
class WebformRemoteFieldsBaseTest extends WebDriverTestBase {

  /**
   * Modules to load.
   *
   * @var string[]
   */
  protected static $modules = [
    'webform_remote_fields',
    'webform',
    'webform_ui',
    'webform_test_element',
  ];

  /**
   * Default theme to use on the tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stable9';

  /**
   * Directory of the config files.
   *
   * @var string
   */
  private static $configDirectory = __DIR__ . '/../../config';

  /**
   * Default permissions for the user that will be created.
   *
   * @var string[]
   */
  protected static $userWebformRemoteFieldPermissions = [
    'administer webform',
    'create webform',
    'edit own webform',
    'administer webform_remote_fields configuration',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(self::$userWebformRemoteFieldPermissions);
    $this->drupalLogin($admin_user);
  }

  /**
   * Helper function to create a webform using the links.
   *
   * @param string $webform
   *   Webform name.
   * @param string $field_type
   *   Custom webform remote field name.
   * @param string $op
   *   The operation to do.
   */
  public function gotoWebform($webform = NULL, $field_type = NULL, $op = 'add'): void {
    if (is_null($webform)) {
      $webform = strtolower($this->randomMachineName());
    }
    if (is_null($field_type)) {
      $field_type = 'remote_field_number';
    }
    $this->drupalGet('/admin/structure/webform/add');
    $this->submitForm(['title' => $webform], 'Save');
    $this->assertSession()
      ->pageTextContains("Webform {$webform} created.");
    $this->drupalGet("/admin/structure/webform/manage/{$webform}/element/{$op}/{$field_type}");
  }

  /**
   * Load the webform from yml config and create it.
   *
   * @param string $webform_id
   *   The name of webform file name.
   *
   * @return string
   *   The if of the form created.
   */
  public function loadWebform($webform_id) {
    $storage = \Drupal::entityTypeManager()->getStorage('webform');
    $file_storage = new FileStorage(static::$configDirectory);
    $values = $file_storage->read($webform_id);
    $webform = $storage->create($values);
    $webform->save();
    return $webform->id();
  }

}
