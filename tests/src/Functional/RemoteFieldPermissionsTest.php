<?php

namespace Drupal\Tests\webform_remote_fields\Functional;

/**
 * Test access on webform_remote_field plugins operations.
 *
 * @group webform_remote_fields
 */
class RemoteFieldPermissionsTest extends WebformRemoteFieldsBaseTest {

  /**
   * The name of the Webform to use on the tests.
   *
   * @var string
   */
  protected $webformName = 'test_webform';

  /**
   * The name of the field to test.
   *
   * @var string
   */
  protected $fieldName = 'number_remote_field_test';

  /**
   * Test if user with properly permissions are able to do all operations.
   */
  public function testUserWithPermissions() {
    $webform = $this->loadWebform('webform.webform.select');

    // Testing creating element.
    $this->drupalGet("/admin/structure/webform/manage/{$webform}/element/add/remote_field_number");

    $this->assertSession()
      ->elementExists('css', 'input[name="properties[api_endpoint]"]');
    $this->assertSession()
      ->elementExists('css', 'input[name="properties[api_result_select_value]"]');

    $this->submitForm(
      [
        'properties[title]' => 'number_remote_field_test',
        'properties[api_endpoint]' => 'http://example.com',
        'properties[api_result_select_value]' => 'value',
      ],
      'Save'
    );
    $this->assertSession()->pageTextContains("{$this->fieldName} has been created.");
  }

  /**
   * Test if user without the properly permissions receive access denied.
   */
  public function testUserWithoutPermission() {
    $admin_user = $this->drupalCreateUser([
      'create webform',
      'edit any webform',
    ]);
    $this->drupalLogin($admin_user);
    $webform = $this->loadWebform('webform.webform.select');

    // Testing creating element.
    $this->drupalGet("/admin/structure/webform/manage/{$webform}/element/add/remote_field_number");
    $this->assertSession()->pageTextContains('You do not have access to edit this field.');
    $this->assertSession()
      ->elementNotExists('css', 'input[name="properties[api_endpoint]"]');
    $this->assertSession()
      ->elementNotExists('css', 'input[name="properties[api_result_select_value]"]');

    // Testing editing element.
    $this->drupalGet("/admin/structure/webform/manage/{$webform}/element/remote_select_1/edit");
    $this->assertSession()->pageTextContains('You do not have access to edit this field.');

    $this->drupalGet("/admin/structure/webform/manage/{$webform}/element/remote_select_1/delete");
    $this->assertSession()->pageTextContains('You do not have access to delete this field.');
  }

}
